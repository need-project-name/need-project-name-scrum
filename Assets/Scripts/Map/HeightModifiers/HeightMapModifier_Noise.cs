using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Noise;

[System.Serializable]
public class HeightNoisePass {
    public float HeightDelta = 1f;
    public float NoiseScale = 1f;
    public int Seed = 0;
    [Range( 1, 16 )]
    public int Octaves = 4;

    [Range( 0f, 1f )]
    public float Persistance = 0.5f;

    [Range( 0f, 4f )]
    public float Lacunarity = 2f;

    public Vector2 Offset;
    public NormalizeMode NormalizeMode = NormalizeMode.Global;
}

public class HeightMapModifier_Noise : BaseHeightMapModifier {
    [SerializeField] List<HeightNoisePass> Passes;


    public override void Execute( TerrainGeneratorConfig globalConfig, int mapResolution, float[,] heightMap, Vector3 heightmapScale, 
        byte[,] biomeMap = null, int biomeIndex = -1, TerrainGeneratorBiomeConfig biome = null ) {
        float halfMapResolution = mapResolution / 2f;

        foreach( var pass in Passes ) {

            //Generate "base" noise map
            float[,] noiseMap = Noise.GenerateNoiseMap( mapResolution, mapResolution, pass.Seed, pass.NoiseScale, pass.Octaves, 
                pass.Persistance, pass.Lacunarity, pass.Offset, pass.NormalizeMode );

            for( int y = 0; y < mapResolution; ++y ) {
                for( int x = 0; x < mapResolution; ++x ) {
                    // skip if we have a biome and this is not our biome
                    if( biomeIndex >= 0 && biomeMap[ x, y ] != biomeIndex )
                        continue;

                    // calculate the new height
                    float newHeight = heightMap[ x, y ] + ( noiseMap[ x, y ] * pass.HeightDelta / heightmapScale.y );

                    // blend based on strength
                    heightMap[ x, y ] = Mathf.Lerp( heightMap[ x, y ], newHeight, Strength );
                }
            }

        }

    }
}
