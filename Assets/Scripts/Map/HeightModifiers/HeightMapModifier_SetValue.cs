using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightMapModifier_SetValue : BaseHeightMapModifier {
    [SerializeField]
    float TargetHeight;

    public override void Execute( TerrainGeneratorConfig globalConfig, int mapResolution, float[,] heightMap, Vector3 heightMapScale, byte[,] biomeMap = null, int biomeIndex = -1, TerrainGeneratorBiomeConfig biome = null ) {
        for( int y = 0; y < mapResolution; ++y ) {
            for( int x = 0; x < mapResolution; ++x ) {

                if( biomeIndex >= 0 && heightMap[ x, y ] != biomeIndex )
                    continue;

                //calculate new height for points
                float newHeight = TargetHeight / heightMapScale.y;

                //blend height based on strength
                heightMap[ x, y ] = Mathf.Lerp( heightMap[ x, y ], newHeight, Strength );

            }
        }
    }
}
