using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightMapModifier_Random : BaseHeightMapModifier {
    [SerializeField]
    float HeightDelta;

    public override void Execute( TerrainGeneratorConfig globalConfig, int mapResolution, float[,] heightMap, Vector3 heightMapScale, byte[,] biomeMap = null, int biomeIndex = -1, TerrainGeneratorBiomeConfig biome = null ) {
        for( int y = 0; y < heightMap.GetLength( 0 ); ++y ) {
            for( int x = 0; x < heightMap.GetLength( 1 ); ++x ) {

                if( biomeIndex >= 0 && heightMap[ x, y ] != biomeIndex )
                    continue;

                float newHeight = heightMap[ x, y ] + ( Random.Range( -HeightDelta, HeightDelta ) / heightMapScale.y );


                //blend based on strength
                heightMap[ x, y ] = Mathf.Lerp( heightMap[ x, y ], newHeight, Strength );

            }
        }
    }
}
