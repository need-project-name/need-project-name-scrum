using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHeightMapModifier : MonoBehaviour
{
    [SerializeField]
    [Range( 0f, 1f )]
    protected float Strength = 1f;

    public virtual void Execute( TerrainGeneratorConfig globalConfig, int mapResolution, float[,] heightMap, Vector3 heightMapScale, byte[,] biomeMap = null, int biomeIndex = -1, TerrainGeneratorBiomeConfig biome = null ) {
        Debug.LogError( "No implementation of Execute function for " + gameObject.name );
    }
}
