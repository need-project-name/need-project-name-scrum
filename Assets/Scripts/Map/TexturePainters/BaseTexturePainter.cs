using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTexturePainter : MonoBehaviour {
    [SerializeField]
    [Range( 0f, 1f )]
    protected float Strength = 1f;

    public virtual void Paint( TerrainGeneratorManager manager, int mapResolution, float[,] heightMap, Vector3 heightmapScale, float[,] slopeMap, float[,,] alphaMaps, int alphaMapResolution, byte[,] biomeMap = null, int biomeIndex = -1, TerrainGeneratorBiomeConfig biome = null ) {
        Debug.LogError( "No implementation of Paint funtion for " + gameObject.name ); 
    }

}
