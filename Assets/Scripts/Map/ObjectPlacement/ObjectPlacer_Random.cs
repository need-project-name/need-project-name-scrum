using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacer_Random : BaseObjectPlacer {

    public override void Place(TerrainGeneratorConfig config, Transform rootObject, int mapResolution, float[,] heightMap, Vector3 heightmapScale, float[,] slopeMap, float[,,] alphaMaps, int alphaMapResolution, byte[,] biomeMap = null, int biomeIndex = -1, TerrainGeneratorBiomeConfig biome = null ) {

        base.Place(config, rootObject, mapResolution, heightMap, heightmapScale, slopeMap, alphaMaps, alphaMapResolution, biomeMap, biomeIndex, biome );
        //get potential locations to spawn objects
        List<Vector3> biomeLocations = GetAllLocationsForBiome( mapResolution, heightMap, biomeIndex, heightmapScale, biomeMap );

        SimpleObjectSpawning( config, rootObject, biomeLocations );

    }
}
