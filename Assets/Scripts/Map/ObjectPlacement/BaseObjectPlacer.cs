using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class PlaceableObjectConfig
{
    public bool HasHeightLimits = false;
    public float MinHeightToSpawn = 0f;
    public float MaxHeightToSpawn = 0f;

    [Range(0f, 1f)] public float Weighting = 1f;
    public List<GameObject> Prefabs;

    public float NormalisedWeighting { get; set; } = 0f;
}

public class BaseObjectPlacer : MonoBehaviour
{
    [SerializeField] protected List<PlaceableObjectConfig> Objects;
    [SerializeField] protected float TargetDensity = 0.1f;
    [SerializeField] protected int MaxSpawnCount = 1000;
    [SerializeField] protected int MaxInvalidLocationSkips = 10;
    [SerializeField] protected float MaxPositionJitter = 0.15f;

    public List<Vector3> GetAllLocationsForBiome(int mapResolution, float[,] heightMap, int biomeIndex, Vector3 heightMapScale, byte[,] biomeMap)
    {
        List<Vector3> locations = new List<Vector3>(mapResolution * mapResolution / 10);

        for (int y = 0; y < mapResolution; ++y)
        {
            for (int x = 0; x < mapResolution; ++x)
            {
                if (biomeMap[x, y] != biomeIndex)
                    continue;

                float height = heightMap[x, y] * heightMapScale.y;

                locations.Add(new Vector3(y * heightMapScale.z, height, x * heightMapScale.x));
            }
        }

        return locations;
    }

    public virtual void Place(TerrainGeneratorConfig config, Transform rootObject, int mapResolution, float[,] heightMap, Vector3 heightmapScale, float[,] slopeMap, float[,,] alphaMaps, int alphaMapResolution, byte[,] biomeMap = null, int biomeIndex = -1, TerrainGeneratorBiomeConfig biome = null)
    {
        // normalise the weightings
        float weightSum = 0f;
        foreach (var obj in Objects)
            weightSum += obj.Weighting;
        foreach (var obj in Objects)
            obj.NormalisedWeighting = obj.Weighting / weightSum;
    }

    protected virtual void SimpleObjectSpawning(TerrainGeneratorConfig config, Transform objectRoot, List<Vector3> candidateLocations)
    {
        foreach (var spawnConfig in Objects)
        {

            // determine the spawn count
            float baseSpawnCount = Mathf.Min(MaxSpawnCount, candidateLocations.Count * TargetDensity);
            int numToSpawn = Mathf.FloorToInt(spawnConfig.NormalisedWeighting * baseSpawnCount);

            int skipCount = 0;
            int numPlaced = 0;
            for (int index = 0; index < numToSpawn; ++index)
            {
                // pick a random prefab
                var prefab = spawnConfig.Prefabs[Random.Range(0, spawnConfig.Prefabs.Count)];
                // pick a random location to spawn at
                int randomLocationIndex = Random.Range(0, candidateLocations.Count);
                Vector3 spawnLocation = candidateLocations[randomLocationIndex];

                bool isValid = true;
                // skip  if outside of height limits
                if (spawnConfig.HasHeightLimits && (spawnLocation.y < spawnConfig.MinHeightToSpawn || spawnLocation.y >= spawnConfig.MaxHeightToSpawn))
                    isValid = false;

                // location is not valid?
                if (!isValid)
                {
                    ++skipCount;
                    --index;

                    if (skipCount >= MaxInvalidLocationSkips)
                        break;

                    continue;
                }
                skipCount = 0;
                ++numPlaced;

                // remove the location if chosen
                candidateLocations.RemoveAt(randomLocationIndex);

                SpawnObject(prefab, spawnLocation, objectRoot);
            }

            Debug.Log($"Placed {numPlaced} objects out of {numToSpawn}");
        }
    }

    protected virtual void SpawnObject(GameObject prefab, Vector3 spawnLocation, Transform objectRoot)
    {
        Quaternion spawnRotation = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
        Vector3 positionOffset = new Vector3(Random.Range(-MaxPositionJitter, MaxPositionJitter),
                                             0,
                                             Random.Range(-MaxPositionJitter, MaxPositionJitter));

        // instantiate the prefab
#if UNITY_EDITOR
        if (Application.isPlaying)
            Instantiate(prefab, spawnLocation + positionOffset, spawnRotation, objectRoot);
        else
        {
            var spawnedGO = PrefabUtility.InstantiatePrefab(prefab, objectRoot) as GameObject;
            spawnedGO.transform.position = spawnLocation + positionOffset;
            spawnedGO.transform.rotation = spawnRotation;
            Undo.RegisterCreatedObjectUndo(spawnedGO, "Placed object");
        }
#else
        Instantiate(prefab, spawnLocation + positionOffset, spawnRotation, objectRoot);
#endif // UNITY_EDITOR 
    }
}