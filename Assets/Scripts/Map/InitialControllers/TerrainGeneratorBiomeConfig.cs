using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BiomeTexture {
    public string Hashcode;
    public Texture2D DiffuseMap;
    public Texture2D NormalMap;
}

[CreateAssetMenu( fileName = "Biome Config", menuName = "Procedural Generation/Biome Configuration", order = -1 )]
public class TerrainGeneratorBiomeConfig : ScriptableObject {
    public string Name;

    [Range( 0f, 1f )]
    public float MinIntensity = 0.5f;

    [Range( 0f, 1f )]
    public float MaxIntensity = 1f;

    [Range( 0f, 1f )]
    public float MinDecayRate = 0.01f;

    [Range( 0f, 1f )]
    public float MaxDecayRate = 0.02f;

    public GameObject HeightModifier;

    public List<BiomeTexture> Textures;

    public GameObject TexturePainter;

    public GameObject ObjectPlacer;
}
