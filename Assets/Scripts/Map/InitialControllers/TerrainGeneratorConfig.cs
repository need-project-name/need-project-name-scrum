using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class TerrainBiomeConfiguration {
    public TerrainGeneratorBiomeConfig Biome;

    [Range( 0f, 1f )]
    public float Weighting = 1f;
}

[CreateAssetMenu( fileName = "TerrainGenerator Config", menuName = "Procedural Generation/TerrainGenerator Configuration", order = -1 )]
public class TerrainGeneratorConfig : ScriptableObject {
    public List<TerrainBiomeConfiguration> Biomes;

    public enum BiomeMapBaseResolution {
        Size_64x64 = 64,
        Size_128x128 = 128,
        Size_256x256 = 256,
        Size_512x512 = 512,
    }

    public BiomeMapBaseResolution BiomeMapResolution = BiomeMapBaseResolution.Size_64x64;

    [Range( 0f, 1f )]
    public float BiomeSeedPointDensity = 0.1f;

    public GameObject InitialHeightModifier;
    public GameObject HeightPostProcessModifier;

    public GameObject TexturePostProcessModifier;

    public int BiomeAmount => Biomes.Count;

    public float TotalWeighting {
        get {
            return Biomes.Sum( b => b.Weighting );
        }
    }
}
