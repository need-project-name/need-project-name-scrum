using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using static TerrainGeneratorConfig;
using System;

#if UNITY_EDITOR
using UnityEditor;
using UnityEngine.SceneManagement;
#endif

public class TerrainGeneratorManager : MonoBehaviour
{
    [SerializeField]
    TerrainGeneratorConfig Config;

    [SerializeField]
    Terrain TargetTerrain;

    Dictionary<string, int> BiomeTextureToTerrainLayerIndex = new Dictionary<string, int>();

#if UNITY_EDITOR
    byte[,] LowResBiomeMap;
    float[,] LowResBiomeWeightings;

    byte[,] HighResBiomeMap;
    float[,] HighResBiomeWeightings;

    float[,] SlopeMap;
#endif

#if UNITY_EDITOR
    public void GenerateWorld()
    {
        //Get MapResolution from TerrainData
        int mapResolution = this.TargetTerrain.terrainData.heightmapResolution;
        int alphaMapResolution = TargetTerrain.terrainData.alphamapResolution;

        // clear out any previously spawned objects
        for (int childIndex = transform.childCount - 1; childIndex >= 0; --childIndex)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                Destroy(transform.GetChild(childIndex).gameObject);
            else
                Undo.DestroyObjectImmediate(transform.GetChild(childIndex).gameObject);
#else
            Destroy(transform.GetChild(childIndex).gameObject);
#endif // UNITY_EDITOR
        }

        //Map Textures correctly
        GenerateTextureMapping();

        //Generate LowRes BiomeMap
        LowRes_BiomeGeneration((int)BiomeMapBaseResolution.Size_64x64);

        //Generate HighRes BiomeMap
        HighRes_BiomeMapGeneration((int)BiomeMapBaseResolution.Size_64x64, mapResolution);

        //Generate HeightMap
        HeightMapModification(mapResolution, alphaMapResolution);

        GenerateTextureLayers();

        //Paint Terrain with Textures
        TerrainTexturePainting(mapResolution, alphaMapResolution);

        PlaceObjectsOnTerrain(mapResolution, alphaMapResolution);
    }

    private void PlaceObjectsOnTerrain(int mapResolution, int alphaMapResolution)
    {
        float[,] heightMap = this.TargetTerrain.terrainData.GetHeights(0, 0, mapResolution, mapResolution);
        float[,,] alphaMaps = this.TargetTerrain.terrainData.GetAlphamaps(0, 0, alphaMapResolution, alphaMapResolution);


        //0-out all layers
        for (int y = 0; y < alphaMapResolution; ++y)
        {
            for (int x = 0; x < alphaMapResolution; ++x)
            {
                for (int layerIndex = 0; layerIndex < this.TargetTerrain.terrainData.alphamapLayers; ++layerIndex)
                {
                    alphaMaps[x, y, layerIndex] = 0;
                }
            }
        }

        for (int biomeIndex = 0; biomeIndex < Config.BiomeAmount; ++biomeIndex)
        {
            var biome = Config.Biomes[biomeIndex].Biome;
            if (biome.ObjectPlacer == null)
                continue;

            BaseObjectPlacer[] placers = biome.ObjectPlacer.GetComponents<BaseObjectPlacer>();

            foreach (var placer in placers)
            {

                placer.Place(Config, transform, mapResolution, heightMap, TargetTerrain.terrainData.heightmapScale, SlopeMap, alphaMaps, alphaMapResolution, HighResBiomeMap, biomeIndex, biome);
            }
        }


    }
#endif
    public int GetLayerForTexture(string randomTexture)
    {
        return BiomeTextureToTerrainLayerIndex[randomTexture];
    }
#if UNITY_EDITOR
    private void TerrainTexturePainting(int mapResolution, int alphaMapResolution)
    {
        float[,] heightMap = TargetTerrain.terrainData.GetHeights(0, 0, mapResolution, mapResolution);

        float[,,] alphaMaps = TargetTerrain.terrainData.GetAlphamaps(0, 0, alphaMapResolution, alphaMapResolution);

        //0 all alpha layers
        for (int y = 0; y < alphaMapResolution; ++y)
        {
            for (int x = 0; x < alphaMapResolution; ++x)
            {
                for (int layerIndex = 0; layerIndex < TargetTerrain.terrainData.alphamapLayers; ++layerIndex)
                {
                    alphaMaps[x, y, layerIndex] = 0;
                }
            }
        }

        //0 all slope layers
        for (int y = 0; y < alphaMapResolution; ++y)
        {
            for (int x = 0; x < alphaMapResolution; ++x)
            {
                for (int layerIndex = 0; layerIndex < TargetTerrain.terrainData.alphamapLayers; ++layerIndex)
                {
                    SlopeMap[x, y] = TargetTerrain.terrainData.GetInterpolatedNormal(x * TargetTerrain.terrainData.heightmapScale.x, y * TargetTerrain.terrainData.heightmapScale.z).y;
                }
            }
        }

        for (int biomeIndex = 0; biomeIndex < Config.BiomeAmount; ++biomeIndex)
        {
            var biome = Config.Biomes[biomeIndex].Biome;
            if (biome.TexturePainter == null)
                continue;

            BaseTexturePainter[] painters = biome.TexturePainter.GetComponents<BaseTexturePainter>();

            foreach (var painter in painters)
            {

                painter.Paint(this, mapResolution, heightMap, TargetTerrain.terrainData.heightmapScale, SlopeMap, alphaMaps, alphaMapResolution, HighResBiomeMap, biomeIndex, biome);
            }
        }

        //run texture post processing
        if (Config.TexturePostProcessModifier != null)
        {

            BaseTexturePainter[] painterModifiers = Config.TexturePostProcessModifier.GetComponents<BaseTexturePainter>();

            foreach (var painter in painterModifiers)
            {
                painter.Paint(this, mapResolution, heightMap, TargetTerrain.terrainData.heightmapScale, SlopeMap, alphaMaps, alphaMapResolution);
            }

        }
        TargetTerrain.terrainData.SetAlphamaps(0, 0, alphaMaps);
    }

    private void GenerateTextureMapping()
    {
        BiomeTextureToTerrainLayerIndex.Clear();

        int layerIndex = 0;
        foreach (var biomeMetaData in Config.Biomes)
        {
            var biome = biomeMetaData.Biome;

            //iterate over textures
            foreach (var texture in biome.Textures)
            {

                BiomeTextureToTerrainLayerIndex[texture.Hashcode] = layerIndex;
                ++layerIndex;
            }
        }
    }

    public void GenerateTextures()
    {
        int mapResolution = this.TargetTerrain.terrainData.heightmapResolution;
        int alphaMapResolution = this.TargetTerrain.terrainData.alphamapResolution;

        if (this.SlopeMap == null)
        {
            this.SlopeMap = new float[alphaMapResolution, alphaMapResolution];

            //generate the slope map
            for (int y = 0; y < alphaMapResolution; ++y)
            {
                for (int x = 0; x < alphaMapResolution; ++x)
                {
                    SlopeMap[x, y] = this.TargetTerrain.terrainData.GetInterpolatedNormal((float)x / alphaMapResolution, (float)y / alphaMapResolution).y;
                }
            }
        }

        GenerateTextureLayers();

        //Map Textures correctly
        GenerateTextureMapping();

        //Paint Terrain with Textures
        TerrainTexturePainting(mapResolution, alphaMapResolution);
    }

    private void GenerateTextureLayers()
    {

        //Clear Existing layers
        if (TargetTerrain.terrainData.terrainLayers == null || TargetTerrain.terrainData.terrainLayers.Length > 0)
        {
            Undo.RecordObject(TargetTerrain, "Clearing previous layers");

            List<string> layersToDelete = new List<string>();

            //get path to asset for each layer
            foreach (var layer in TargetTerrain.terrainData.terrainLayers)
            {
                if (layer == null)
                    continue;

                layersToDelete.Add(AssetDatabase.GetAssetPath(layer.GetInstanceID()));
            }

            //Remove all links to layer
            TargetTerrain.terrainData.terrainLayers = null;

            foreach (var layerAsset in layersToDelete)
            {
                if (string.IsNullOrEmpty(layerAsset))
                    continue;

                AssetDatabase.DeleteAsset(layerAsset);
            }

            Undo.FlushUndoRecordObjects();
        }

        string scenePath = System.IO.Path.GetDirectoryName(SceneManager.GetActiveScene().path);
        List<TerrainLayer> newLayers = new List<TerrainLayer>();
        foreach (var biomeMetaData in Config.Biomes)
        {
            var biome = biomeMetaData.Biome;

            //iterate over textures
            foreach (var texture in biome.Textures)
            {
                //create texture layer
                TerrainLayer textureLayer = new TerrainLayer();
                textureLayer.diffuseTexture = texture.DiffuseMap;
                textureLayer.normalMapTexture = texture.NormalMap;


                //save as asset
                string layerPath = System.IO.Path.Combine(scenePath, "Layer_" + biome.Name + "_" + texture.Hashcode);
                AssetDatabase.CreateAsset(textureLayer, layerPath);

                BiomeTextureToTerrainLayerIndex[texture.Hashcode] = newLayers.Count;
                newLayers.Add(textureLayer);
            }
        }

        Undo.RecordObject(TargetTerrain.terrainData, "Updating terrain layers");
        TargetTerrain.terrainData.terrainLayers = newLayers.ToArray();
    }

    void LowRes_BiomeGeneration(int resolution)
    {
        //Allocate the biome map and weightings maps
        LowResBiomeMap = new byte[resolution, resolution];
        LowResBiomeWeightings = new float[resolution, resolution];

        //setup space for the seed points
        int numberSeedPoints = Mathf.FloorToInt(resolution * resolution * Config.BiomeSeedPointDensity);
        List<byte> biomesList = new List<byte>(numberSeedPoints);

        //Populate BiomesToSpawn based on weightings
        float totalWeighting = Config.TotalWeighting;
        for (int biomeIndex = 0; biomeIndex < Config.BiomeAmount; ++biomeIndex)
        {
            int numEntries = Mathf.RoundToInt(numberSeedPoints * Config.Biomes[biomeIndex].Weighting / totalWeighting);

            for (int entryIndex = 0; entryIndex < numEntries; ++entryIndex)
            {
                biomesList.Add((byte)biomeIndex);
            }
        }

        //Spawn individual Biomes
        while (biomesList.Count > 0)
        {
            int seedPointIndex = UnityEngine.Random.Range(0, biomesList.Count);

            //extract biomeIndex 
            byte biomeIndex = biomesList[seedPointIndex];

            //remove seed point
            biomesList.RemoveAt(seedPointIndex);

            SpawnIndividualBiome(biomeIndex, resolution);
        }

        Texture2D biomeTextureMap = new Texture2D(resolution, resolution);
        for (int i = 0; i < resolution; ++i)
        {
            for (int j = 0; j < resolution; ++j)
            {
                float hue = ((float)LowResBiomeMap[i, j] / (float)Config.BiomeAmount);
                biomeTextureMap.SetPixel(i, j, Color.HSVToRGB(hue, 0.75f, 0.75f));
            }
        }
        biomeTextureMap.Apply();
        System.IO.File.WriteAllBytes("BiomeTextureMap.png", biomeTextureMap.EncodeToPNG());
    }

    Vector2Int[] NeighbourOffsets = new Vector2Int[] {
        new Vector2Int (0,1),
        new Vector2Int (0,-1),
        new Vector2Int (1,0),
        new Vector2Int (-1,0),
        new Vector2Int (1,1),
        new Vector2Int (-1,-1),
        new Vector2Int (1,-1),
        new Vector2Int (-1,1)
    };


    /// <summary>
    /// Using ooze-based generation. Source: https://www.procjam.com/tutorials/en/ooze/
    /// </summary>
    /// <param name="biomeIndex"></param>
    /// <param name="resolution"></param>
    private void SpawnIndividualBiome(byte biomeIndex, int mapResolution)
    {
        //cache biome config
        TerrainGeneratorBiomeConfig biomeConfig = Config.Biomes[biomeIndex].Biome;

        //pick spawn location
        Vector2Int spawnLocation = new Vector2Int(UnityEngine.Random.Range(0, mapResolution), UnityEngine.Random.Range(0, mapResolution));

        //pick the starting intensity
        float startIntensity = UnityEngine.Random.Range(biomeConfig.MinIntensity, biomeConfig.MaxIntensity);

        //Setup working list
        Queue<Vector2Int> workingList = new Queue<Vector2Int>();
        workingList.Enqueue(spawnLocation);

        //setup the visited map and target intensity map
        bool[,] visited = new bool[mapResolution, mapResolution];
        float[,] targetIntensity = new float[mapResolution, mapResolution];

        //set starting intensity
        targetIntensity[spawnLocation.x, spawnLocation.y] = startIntensity;


        //start ooze based generation
        while (workingList.Count > 0)
        {
            Vector2Int workingLocation = workingList.Dequeue();

            //set the biome
            LowResBiomeMap[workingLocation.x, workingLocation.y] = biomeIndex;
            visited[workingLocation.x, workingLocation.y] = true;
            LowResBiomeWeightings[workingLocation.x, workingLocation.y] = targetIntensity[workingLocation.x, workingLocation.y];


            //traverse neighbours
            for (int neighbourIndex = 0; neighbourIndex < NeighbourOffsets.Length; ++neighbourIndex)
            {
                Vector2Int neighbourLocation = workingLocation + NeighbourOffsets[neighbourIndex];

                //skip if invalid
                if (neighbourLocation.x < 0 || neighbourLocation.y < 0 || neighbourLocation.x >= mapResolution || neighbourLocation.y >= mapResolution)
                    continue;

                //skip if already visited
                if (visited[neighbourLocation.x, neighbourLocation.y])
                    continue;

                //flag location as visited
                visited[neighbourLocation.x, neighbourLocation.y] = true;

                //working out neighbour weightings
                float decayAmount = UnityEngine.Random.Range(biomeConfig.MinDecayRate, biomeConfig.MaxDecayRate) * NeighbourOffsets[neighbourIndex].magnitude;
                float neighbourStrength = targetIntensity[workingLocation.x, workingLocation.y] - decayAmount;
                targetIntensity[neighbourLocation.x, neighbourLocation.y] = neighbourStrength;

                if (neighbourStrength <= 0)
                    continue;

                workingList.Enqueue(neighbourLocation);
            }
        }
    }

    private void HighRes_BiomeMapGeneration(int lowResMapSize, int highResMapSize)
    {
        HighResBiomeMap = new byte[highResMapSize, highResMapSize];
        HighResBiomeWeightings = new float[highResMapSize, highResMapSize];

        //calculate map scale
        float mapScale = (float)lowResMapSize / (float)highResMapSize;

        //calculate highres map
        for (int y = 0; y < highResMapSize; ++y)
        {
            int lowResY = Mathf.FloorToInt(y * mapScale);
            float yFraction = y * mapScale - lowResY;

            for (int x = 0; x < highResMapSize; ++x)
            {
                int lowResX = Mathf.FloorToInt(x * mapScale);
                float xFraction = x * mapScale - lowResX;


                HighResBiomeMap[x, y] = CalculateHighResBiomeIndex(lowResMapSize, lowResX, lowResY, xFraction, yFraction);
            }
        }



        Texture2D highResBiomeTextureMap = new Texture2D(highResMapSize, highResMapSize);
        for (int y = 0; y < highResMapSize; ++y)
        {
            for (int x = 0; x < highResMapSize; ++x)
            {
                float hue = ((float)HighResBiomeMap[x, y] / (float)Config.BiomeAmount);
                highResBiomeTextureMap.SetPixel(x, y, Color.HSVToRGB(hue, 0.75f, 0.75f));
            }
        }
        highResBiomeTextureMap.Apply();
        System.IO.File.WriteAllBytes("HighRes_BiomeTextureMap.png", highResBiomeTextureMap.EncodeToPNG());
    }

    private byte CalculateHighResBiomeIndex(int lowResMapSize, int lowResX, int lowResY, float fractionX, float fractionY)
    {
        float A = LowResBiomeMap[lowResX, lowResY];
        float B = (lowResX + 1) < lowResMapSize ? LowResBiomeMap[lowResX + 1, lowResY] : A;
        float C = (lowResY + 1) < lowResMapSize ? LowResBiomeMap[lowResX, lowResY + 1] : A;
        float D = 0;

        if ((lowResX + 1) >= lowResMapSize)
            D = C;
        else if ((lowResY + 1) >= lowResMapSize)
            D = B;
        else
            D = LowResBiomeMap[lowResX + 1, lowResY + 1];

        //bilinear filtering
        float filteredIndex = A * (1 - fractionX) * (1 - fractionY) + B * fractionX * (1 - fractionY) *
                                     C * fractionY * (1 - fractionX) + D * fractionX * fractionY;


        //build an array of the possible biomes based on the values used to interpolate
        float[] candidateBiomes = new float[] { A, B, C, D };

        //find neighbouring biome closest to interpolated biome
        float bestBiomeCandidate = -1f;
        float biomeDelta = float.MaxValue;

        for (int biomeIndex = 0; biomeIndex < candidateBiomes.Length; ++biomeIndex)
        {
            float delta = Mathf.Abs(filteredIndex - candidateBiomes[biomeIndex]);


            if (delta < biomeDelta)
            {
                biomeDelta = delta;
                bestBiomeCandidate = candidateBiomes[biomeIndex];
            }
        }


        return (byte)Mathf.RoundToInt(bestBiomeCandidate);
    }

    private void HeightMapModification(int mapResolution, int alphaMapResolution)
    {

        float[,] heightMap = TargetTerrain.terrainData.GetHeights(0, 0, mapResolution, mapResolution);

        //initial Heightmap shenanigans
        if (Config.InitialHeightModifier != null)
        {
            BaseHeightMapModifier[] modifiers = Config.InitialHeightModifier.GetComponents<BaseHeightMapModifier>();
            foreach (var modifier in modifiers)
            {
                modifier.Execute(Config, mapResolution, heightMap, TargetTerrain.terrainData.heightmapScale);
            }
        }

        //generate heightmaps for each biome
        for (int biomeIndex = 0; biomeIndex < Config.BiomeAmount; ++biomeIndex)
        {
            TerrainGeneratorBiomeConfig biome = Config.Biomes[biomeIndex].Biome;

            if (biome.HeightModifier == null)
                continue;

            BaseHeightMapModifier[] modifiers = biome.HeightModifier.GetComponents<BaseHeightMapModifier>(); ;

            foreach (var modifier in modifiers)
            {
                modifier.Execute(Config, mapResolution, heightMap, TargetTerrain.terrainData.heightmapScale, HighResBiomeMap, biomeIndex, biome);
            }
        }

        //PostProcessing-Shenanigans
        if (Config.HeightPostProcessModifier != null)
        {
            BaseHeightMapModifier[] modifiers = Config.HeightPostProcessModifier.GetComponents<BaseHeightMapModifier>();

            foreach (var modifier in modifiers)
            {
                modifier.Execute(Config, mapResolution, heightMap, TargetTerrain.terrainData.heightmapScale);
            }
        }


        SlopeMap = new float[alphaMapResolution, alphaMapResolution];
        //generate the slope map
        for (int y = 0; y < alphaMapResolution; ++y)
        {
            for (int x = 0; x < alphaMapResolution; ++x)
            {
                SlopeMap[x, y] = this.TargetTerrain.terrainData.GetInterpolatedNormal((float)x / alphaMapResolution, (float)y / alphaMapResolution).y;
            }
        }

        this.TargetTerrain.terrainData.SetHeights(0, 0, heightMap);
    }

#endif
}