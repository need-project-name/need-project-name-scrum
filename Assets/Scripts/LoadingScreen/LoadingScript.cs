using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LoadingScript : MonoBehaviour
{
    public Slider progressBar;
    public Text infoText;

    private void Start()
    {
        showTooltip();
        LoadAsyncScene(1);
    }

    private void showTooltip()
    {
        var textNumber = Random.Range(0, 5);

        switch (textNumber)
        {
            case 0:
                infoText.text = "Mit Shift kannst du schneller Sprinten";
                break;
            case 1:
                infoText.text = "Mit Links Klick greifst du an";
                break;
            case 2:
                infoText.text = "Schau in die Einstellung, falls du Performance Probleme hast";
                break;
            case 3:
                infoText.text = "Frag mich nicht, ich habe selber keine Ahnung";
                break;
            case 4:
                infoText.text = "Its not a Bug, its a feature";
                break;
            case 5:
                infoText.text = "Mit der Leertaste kannst du Springen";
                break;
        }
    }

    private void LoadAsyncScene(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            progressBar.value = operation.progress;
            yield return null;
        }
    }
}
