using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    private static int BaseHealth = 100;

    private int health;

    private void Start()
    {
        health = BaseHealth;
    }

    public static int baseHealth
    {
        get => BaseHealth;
        set => BaseHealth = value;
    }

    public int Health
    {
        get => health;
        set => health = value;
    }
}
