using Assets.Scripts.Menu;
using ProjectZ.Waves;
using UnityEngine;
using UnityEngine.AI;
using Vector3 = UnityEngine.Vector3;

public class EnemyFollow : MonoBehaviour {
    private AudioSource playerHit;
    public NavMeshAgent enemy;
    public Transform enemyposition;
    private Vector3 enemystartposition;
    private float dist;
    private float attackTimer;
    private float nextattack;
    private bool coolDown;

    private PlayerHealth playerhp;
    private Transform playerposition;

    [SerializeField] private AudioSource attackSound;

    private AudioSource femaleHit;
    private AudioSource maleHit;

    private VoiceController voiceController;

    private void Start() {
        GameObject player = GameObject.FindWithTag( "Player" );
        voiceController = player.GetComponent<VoiceController>();
        playerposition = player.transform;
        playerhp = player.GetComponent<PlayerHealth>();

        femaleHit = GameObject.Find( "FemaleHit" ).GetComponent<AudioSource>();
        maleHit = GameObject.Find( "MaleHit" ).GetComponent<AudioSource>();

        enemystartposition = enemyposition.position;
        attackTimer = 1.0f;
        coolDown = false;
    }

    private void Update() {
        if( !DeathScreenController.IsPaused ) {

            if( enemy.transform.position.y < -100 ) {

                Destroy( enemy.transform.parent.gameObject );
                GameObject.Find( "WaveController" ).GetComponent<WaveController>().EnemieDies( true );

            }

            dist = Vector3.Distance( playerposition.position, transform.position );


            transform.LookAt( playerposition );
            if( dist >= 2.6 ) {
                enemy.isStopped = false;
                enemy.SetDestination( playerposition.position );
            } else {
                enemy.isStopped = true;
            }

            if( dist <= 2.6 ) {
                if( !coolDown && Time.time > nextattack ) {
                    nextattack = Time.time + attackTimer;
                    playerhp.playerhp -= 5;

                    attackSound.Play();
                    PlayerSound();
                }
            }
        } else {
            enemy.isStopped = true;
        }
    }

    private void PlayerSound() {
        if( voiceController.IsFemale ) {
            femaleHit.Play();
        } else {
            maleHit.Play();
        }
    }

}