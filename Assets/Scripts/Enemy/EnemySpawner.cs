using ProjectZ.Waves;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    private bool spawnOneEnemy = false;
    private void Start()
    {
        WaveController.enemySpawner.Add(this);
    }
    private void Update()
    {
        if (spawnOneEnemy)
        {
            var transform1 = transform;
            Instantiate(enemy, transform1.position, transform1.rotation);

            spawnOneEnemy = false;
        }
    }

    public void SpawnEnemy()
    {
        spawnOneEnemy = true;
    }
}
