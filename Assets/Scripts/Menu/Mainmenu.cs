using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Mainmenu : MonoBehaviour
{
    public Button startButton;
    public Button scoreboardButton;
    public Button settingsButton;
    public Button quitButton;

    private void Start()
    {
        startButton.GetComponent<Button>().onClick.AddListener(startGame);
        quitButton.GetComponent<Button>().onClick.AddListener(quitGame);
        scoreboardButton.GetComponent<Button>().onClick.AddListener(ScoreboardButton);
        settingsButton.GetComponent<Button>().onClick.AddListener(settings);
    }

    private void startGame()
    {
        SceneManager.LoadScene(4);
    }

    private void quitGame()
    {
        Application.Quit();
    }

    private void ScoreboardButton()
    {
        SceneManager.LoadScene(2);
    }

    private void settings()
    {
        SceneManager.LoadSceneAsync(3);
    }
}
