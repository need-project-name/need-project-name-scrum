﻿using ProjectZ.Scoreboards;
using ProjectZ.Waves;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Menu
{
    public class DeathScreenController : MonoBehaviour
    {
        public static bool IsPaused = false;

        private int playerScore;
        public Button playAgain;
        public Button mainMenu;
        public Canvas uIGameobject;
        [SerializeField] public Canvas deathScreengameobject;
        private TextMeshProUGUI TextBoxScore;
        private TextMeshProUGUI TextBoxName;
            
            // Use this for initialization
        void Start()
        {
            TextBoxName = GameObject.Find("PlayerScoreName_Text").GetComponent<TextMeshProUGUI>();
            TextBoxScore = GameObject.Find("PlayerScoreNumber_Text").GetComponent<TextMeshProUGUI>();
            Debug.Log("DeathScreen Start");
            playAgain.GetComponent<Button>().onClick.AddListener(PlayAgain);
            mainMenu.GetComponent<Button>().onClick.AddListener(MainMenu);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ShowDeathScreen( int score )
        {
            IsPaused = true;
            Cursor.lockState = CursorLockMode.None;
            playerScore = score;
            TextBoxScore.text = playerScore.ToString("D5");
            uIGameobject.enabled = false;
            deathScreengameobject.enabled = true;

        }

        public void HideDeathScreen()
        {
            IsPaused = false;
            uIGameobject.enabled = true;
            deathScreengameobject.enabled = false;
        }

        void SaveScore()
        {
            string name = TextBoxName.text;
            if(name == "​")//Weil der String warum auch immer nicht Empty ist.
                name = "Unknown";
            ScoreboardEntryData score = new ScoreboardEntryData() { entryName = name, entryScore = playerScore };
            Scoreboard.AddEntry(score);
        }
        public void PlayAgain()
        {
            Debug.Log("Play Again");
            //Einfacher die Scene neuzuladen anstelle alles zurückzusetzten
            SaveScore();
            WaveController.enemySpawner.Clear();
            SceneManager.LoadScene(4);
        }
        public void MainMenu()
        {
            SaveScore();
            SceneManager.LoadScene(0);
        }


    }
}