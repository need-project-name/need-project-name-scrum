﻿using System.IO;
using ProjectZ.Settings;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class FPSDisplay : MonoBehaviour
    {
        private string SavePath => $"{Application.persistentDataPath}/settings.json";
        private bool showFPS;
        
        public int avgFrameRate;
        public Text display_Text;

        public void Start()
        {
            if (!File.Exists(SavePath))
            {
                showFPS = false;
            }
            else
            {
               showFPS = GetSettings().showFPS; 
            }
            
        }

        public void Update()
        {
            if (showFPS)
            {
                display_Text.text = ((int)(1f / Time.unscaledDeltaTime)) + " FPS";
            }
        }
        
        private SettingsObjectSaveData GetSettings()
        {
            using (StreamReader stream = new StreamReader(SavePath))
            {
                string json = stream.ReadToEnd();
                return JsonUtility.FromJson<SettingsObjectSaveData>(json);
            }
        }
    }
}