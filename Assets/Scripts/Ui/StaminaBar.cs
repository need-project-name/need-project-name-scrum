using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static PlayerMotor;



public class StaminaBar : MonoBehaviour
{
    public Slider staminabar;
    public bool allowSprinting;
    
    private PlayerMotor motor;
    private PlayerHealth playerhp;
    
    private float staminaTimer;
    private float nextStaminaLoss;
    private bool coolDown;

    private void Start()
    {
        GameObject player = GameObject.FindWithTag("Player");
        motor = player.GetComponent<PlayerMotor>();
        playerhp = player.GetComponent<PlayerHealth>();
        
        coolDown = false;
        staminaTimer = 0.5f;
        staminabar.value = playerhp.playerstamina;
    }

    // Update is called once per frame
    void Update()
    {
        if (motor.sprinting)
        {
            if (!coolDown && Time.time > nextStaminaLoss)
            {
                if (playerhp.playerstamina > 0)
                {
                    nextStaminaLoss = Time.time + staminaTimer;
                    playerhp.playerstamina = playerhp.playerstamina - 10f;
                    staminabar.value = playerhp.playerstamina;
                }
            }
        }

        if (playerhp.playerstamina <= 0)
        {
            allowSprinting = false;
        }
        else if (playerhp.playerstamina > 0)
        {
            allowSprinting = true;
        }
        if (!motor.sprinting && playerhp.playerstamina < 100) 
        {
            refillStaminaBar();
        }

    }

    public void refillStaminaBar()
        //refills staminabar
    {
        if (!coolDown && Time.time > nextStaminaLoss)
        {
            nextStaminaLoss = Time.time + staminaTimer;
            playerhp.playerstamina = playerhp.playerstamina + 10f;
            staminabar.value = playerhp.playerstamina;
        }
    }
}

