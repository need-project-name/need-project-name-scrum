using System;
using UnityEngine;
using UnityEngine.UI;

public class AmmoStatus : MonoBehaviour
{
    [SerializeField] Text status;
    [SerializeField] Text info;
    [SerializeField] private GameObject player;
    private AmmoDao _ammoDao;

    // Start is called before the first frame update
    void Start()
    {
        _ammoDao = player.GetComponent<AmmoDao>();
    }

    // Update is called once per frame
    void Update()
    {
        status.text = _ammoDao.CurrentMagazinAmmo + "/" + _ammoDao.CurrentReserveAmmo;

        if (_ammoDao.CurrentReserveAmmo == 0 && _ammoDao.CurrentMagazinAmmo == 0)
        {
            info.text = "Keine Munition mehr!";
            status.color = Color.red;
        }
        else if (_ammoDao.CurrentMagazinAmmo == 0)
        {
            info.text = "Dein Magazin ist leer!";
            status.color = Color.black;
        }
        else
        {
            info.text = "";
            status.color = Color.black;
        }
    }
}