using System.Collections;
using System.Collections.Generic;
using System.IO;
using ProjectZ.Settings;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerAudioMixer : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer;
    private string SavePath => $"{Application.persistentDataPath}/settings.json";

    // Start is called before the first frame update
    void Start()
    {
        if (File.Exists(SavePath))
        {
            audioMixer.SetFloat("MasterVolume", GetSettings().masterVolume);
            audioMixer.SetFloat("MusicVolume", GetSettings().musicVolume);
            audioMixer.SetFloat("WeaponVolume", GetSettings().weaponVolume);
        }
    }

    private SettingsObjectSaveData GetSettings()
    {
        using (StreamReader stream = new StreamReader(SavePath))
        {
            string json = stream.ReadToEnd();
            return JsonUtility.FromJson<SettingsObjectSaveData>(json);
        }
    }
}
