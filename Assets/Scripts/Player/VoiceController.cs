using System.IO;
using ProjectZ.Settings;
using UnityEngine;

public class VoiceController : MonoBehaviour
{
    private string SavePath => $"{Application.persistentDataPath}/settings.json";

    private bool isFemale;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!File.Exists(SavePath))
        {
            isFemale = false;
        }
        else
        {
            isFemale = GetSettings().isFemale; 
        }
    }

    public bool IsFemale => isFemale;

    private SettingsObjectSaveData GetSettings()
    {
        using (StreamReader stream = new StreamReader(SavePath))
        {
            string json = stream.ReadToEnd();
            return JsonUtility.FromJson<SettingsObjectSaveData>(json);
        }
    }
}
