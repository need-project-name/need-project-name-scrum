using ProjectZ.Waves;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public Slider healtBar;
    
    public float playerhp = 1000f;
    public float playerstamina = 100f;

    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        healtBar.value = playerhp;
        if (playerhp <= 0f)
        {
            GameObject.Find("WaveController").GetComponent<WaveController>().PlayerDies();
        }

    }
}