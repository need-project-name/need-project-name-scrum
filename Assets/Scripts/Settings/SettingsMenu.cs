using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Slider = UnityEngine.UIElements.Slider;

namespace ProjectZ.Settings
{
    public class SettingsMenu : MonoBehaviour
    {
        public Button abbrechenButton;
        public Button saveAndQuitButton;

        [SerializeField] private Dropdown displayMode;
        [SerializeField] private Dropdown resolutionToggle;
        [SerializeField] private Dropdown qualityToggle;
        [SerializeField] private Toggle vSyncToggle;
        [SerializeField] private Toggle showFPS;
        [SerializeField] private Toggle isFemale;

        [SerializeField] private UnityEngine.UI.Slider masterVolumeSlider;
        [SerializeField] private InputField masterVolumeInput;
        [SerializeField] private UnityEngine.UI.Slider musicVolumeSlider;
        [SerializeField] private InputField musicVolumeInput;
        [SerializeField] private UnityEngine.UI.Slider weaponVolumeSlider;
        [SerializeField] private InputField weaponVolumeInput;

        [SerializeField] private AudioMixer audioMixer;

        private Resolution[] _resolutions;
        private List<string> resOptions = new List<string>();
        // Start is called before the first frame update
        private void Start()
        {
            //Exit buttons
            saveAndQuitButton.GetComponent<Button>().onClick.AddListener(saveAndQuit);
            abbrechenButton.GetComponent<Button>().onClick.AddListener(discard);
            
            //Settings Options
            displayMode.GetComponent<Dropdown>().onValueChanged.AddListener(setDisplayMode);
            resolutionToggle.GetComponent<Dropdown>().onValueChanged.AddListener(SetResolution);
            qualityToggle.GetComponent<Dropdown>().onValueChanged.AddListener(setQuality);
            vSyncToggle.GetComponent<Toggle>().onValueChanged.AddListener(setVSync);

            //Volume Slider Events
            masterVolumeSlider.GetComponent<UnityEngine.UI.Slider>().onValueChanged.AddListener(masterVolume);
            musicVolumeSlider.GetComponent<UnityEngine.UI.Slider>().onValueChanged.AddListener(musicVolume);
            weaponVolumeSlider.GetComponent<UnityEngine.UI.Slider>().onValueChanged.AddListener(weaponVolume);
            
            //Volume Input Change
            masterVolumeInput.GetComponent<InputField>().onValueChanged.AddListener(masterVolume);
            musicVolumeInput.GetComponent<InputField>().onValueChanged.AddListener(musicVolume);
            weaponVolumeInput.GetComponent<InputField>().onValueChanged.AddListener(weaponVolume);

            FillList();

            SettingsObjectSaveData settings = GetSettings();
            UpdateUI(settings);
        }

        private void OnDestroy()
        {
            //Exit buttons
            saveAndQuitButton.GetComponent<Button>().onClick.RemoveListener(saveAndQuit);
            abbrechenButton.GetComponent<Button>().onClick.RemoveListener(discard);
            
            //Settings Options
            displayMode.GetComponent<Dropdown>().onValueChanged.RemoveListener(setDisplayMode);
            resolutionToggle.GetComponent<Dropdown>().onValueChanged.RemoveListener(SetResolution);
            qualityToggle.GetComponent<Dropdown>().onValueChanged.AddListener(setQuality);
            vSyncToggle.GetComponent<Toggle>().onValueChanged.RemoveListener(setVSync);
            
            //Volume Slider Events
            masterVolumeSlider.GetComponent<UnityEngine.UI.Slider>().onValueChanged.RemoveListener(masterVolume);
            musicVolumeSlider.GetComponent<UnityEngine.UI.Slider>().onValueChanged.RemoveListener(musicVolume);
            weaponVolumeSlider.GetComponent<UnityEngine.UI.Slider>().onValueChanged.RemoveListener(weaponVolume);
            
            //Volume Input Change
            masterVolumeInput.GetComponent<InputField>().onValueChanged.RemoveListener(masterVolume);
            musicVolumeInput.GetComponent<InputField>().onValueChanged.RemoveListener(musicVolume);
            weaponVolumeInput.GetComponent<InputField>().onValueChanged.RemoveListener(weaponVolume);
        }

        #region Events
        private void discard()
        {
            //Changes should/will not be saved and change scene back to menu
            SceneManager.LoadScene(0);
        }
        private void saveAndQuit()
        {
            SettingsObjectSaveData settings = new SettingsObjectSaveData() { displayMode = displayMode.value, resolution = resolutionToggle.value, quality = qualityToggle.value, VSyncEnabled = vSyncToggle.isOn, showFPS = showFPS.isOn, masterVolume = masterVolumeSlider.value, musicVolume = musicVolumeSlider.value, weaponVolume = weaponVolumeSlider.value, isFemale = isFemale.isOn};
            SaveSettings(settings);
            //Changed should be saved and afterwards change scene to menu
            SceneManager.LoadScene(0);
        }

        private void setVSync(bool isVsync)
        {
            if (isVsync)
            {
                QualitySettings.vSyncCount = 1;
            }
            else
            {
                QualitySettings.vSyncCount = 0;
            }
        }

        private void setDisplayMode(int index)
        {
            switch (index)
            {
                case 0:
                    Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
                    break;
                case 1:
                    Screen.fullScreenMode = FullScreenMode.MaximizedWindow;
                    break;
                case 2:
                    Screen.fullScreenMode = FullScreenMode.Windowed;
                    break;
                default:
                    Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
                    break;
            }
        }

        private void setQuality(int index)
        {
            switch (index)
            {
                //very low
                case 0:
                    QualitySettings.SetQualityLevel(0,true);
                    break;
                
                //low
                case 1:
                    QualitySettings.SetQualityLevel(1,true);
                    break;
                
                //medium
                case 2:
                    QualitySettings.SetQualityLevel(2,true);
                    break;
                
                //high
                case 3:
                    QualitySettings.SetQualityLevel(3,true);
                    break;
                
                //very high
                case 4:
                    QualitySettings.SetQualityLevel(4,true);
                    break;
                
                //ultra
                case 5:
                    QualitySettings.SetQualityLevel(5,true);
                    break;
            }
        }

        private void masterVolume(float val)
        {
            masterVolumeInput.text = val.ToString();
            changeMasterVolume(val);
        }

        private void masterVolume(string input)
        {
            if (!"-".Equals(input) && !"".Equals(input))
            {
                masterVolumeSlider.value = Single.Parse(input);
                changeMasterVolume(Single.Parse(input));
            }
        }

        private void musicVolume(float val)
        {
            musicVolumeInput.text = val.ToString();
            changeMusicVolume(val);
        }
        
        private void musicVolume(string input)
        {
            if (!"-".Equals(input) && !"".Equals(input))
            {
                musicVolumeSlider.value = Single.Parse(input);
                changeMusicVolume(Single.Parse(input));
            }
        }

        private void weaponVolume(float val)
        {
            weaponVolumeInput.text = val.ToString();
            changeWeaponVolume(val);
        }

        private void weaponVolume(string input)
        {
            if (!"-".Equals(input) && !"".Equals(input))
            {
                weaponVolumeSlider.value = Single.Parse(input);
                changeWeaponVolume(Single.Parse(input));
            }
        }
        
        #endregion
        
        #region Settings

        public void UpdateUI(SettingsObjectSaveData settingsSaveData)
        {
            displayMode.value = settingsSaveData.displayMode;
            resolutionToggle.value = settingsSaveData.resolution;
            qualityToggle.value = settingsSaveData.quality;
            vSyncToggle.isOn = settingsSaveData.VSyncEnabled;
            showFPS.isOn = settingsSaveData.showFPS;
            isFemale.isOn = settingsSaveData.isFemale;
            masterVolumeInput.text = settingsSaveData.masterVolume.ToString();
            masterVolumeSlider.value = settingsSaveData.masterVolume;
            musicVolumeInput.text = settingsSaveData.musicVolume.ToString();
            masterVolumeSlider.value = settingsSaveData.musicVolume;
            weaponVolumeInput.text = settingsSaveData.weaponVolume.ToString();
            weaponVolumeSlider.value = settingsSaveData.weaponVolume;
        }
        #endregion
        
        #region SettingsLoadAndSave
        private string SavePath => $"{Application.persistentDataPath}/settings.json";

        private SettingsObjectSaveData GetSettings()
        {
            if (!File.Exists(SavePath))
            {
                File.Create(SavePath).Dispose();
                return new SettingsObjectSaveData();
            }
            using (StreamReader stream = new StreamReader(SavePath))
            {
                string json = stream.ReadToEnd();
                return JsonUtility.FromJson<SettingsObjectSaveData>(json);
            }
        }
        private void SaveSettings(SettingsObjectSaveData settingsObjectSaveData)
        {
            using (StreamWriter stream = new StreamWriter(SavePath))
            {
                string json = JsonUtility.ToJson(settingsObjectSaveData, true);
                stream.Write(json);
            }
        }
        
        #endregion
        
        #region private methods
        
        private void FillList()
        {
            _resolutions = Screen.resolutions;

            int currentResIndex = 0;
            for (int i = 0; i < _resolutions.Length; i++)
            {
                string option = _resolutions[i].width + " x " + _resolutions[i].height + " x " + _resolutions[i].refreshRate + "hz";
                resOptions.Add(option);
                
                if (_resolutions[i].width == Screen.currentResolution.width && 
                    _resolutions[i].width == Screen.currentResolution.width &&
                    _resolutions[i].refreshRate == Screen.currentResolution.refreshRate)
                {
                    currentResIndex = i;
                }
            }
            
            resolutionToggle.AddOptions(resOptions);
            resolutionToggle.value = currentResIndex;
            resolutionToggle.RefreshShownValue();
        }

        private void SetResolution(int index)
        {
            int width = _resolutions[index].width;
            int height = _resolutions[index].height;
            int refreshRate = _resolutions[index].refreshRate;
            Screen.SetResolution(width,height,Screen.fullScreenMode,refreshRate);
        }

        private void changeMasterVolume(float volume)
        {
            audioMixer.SetFloat("MasterVolume", volume);
        }
        
        private void changeMusicVolume(float volume)
        {
            audioMixer.SetFloat("MusicVolume", volume);
        }
        
        private void changeWeaponVolume(float volume)
        {
            audioMixer.SetFloat("WeaponVolume", volume);
        }

        #endregion
    }
}
