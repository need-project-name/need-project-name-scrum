using System;
using UnityEngine;

namespace ProjectZ.Settings
{
    [Serializable]
    public struct SettingsObjectSaveData
    {
        public int displayMode;
        public int resolution;
        public int quality;
        public bool VSyncEnabled;
        public bool showFPS;
        public bool isFemale;
        public float masterVolume;
        public float musicVolume;
        public float weaponVolume;
    }
}