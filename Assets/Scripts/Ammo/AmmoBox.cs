using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MonoBehaviour
{
    public static readonly List<AmmoBox> AllAmmoBoxes = new List<AmmoBox>();

    void OnEnable() => AllAmmoBoxes.Add(this);
    
    void OnDisable() => AllAmmoBoxes.Remove(this);
}
