using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBoxSpawner : MonoBehaviour
{
    public GameObject ammoBox;
    
    // Start is called before the first frame update
    void Start()
    {
        var transform1 = transform;
        Instantiate(ammoBox, transform1.position,transform1.rotation);
        
    }
}
