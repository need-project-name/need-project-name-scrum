using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoDao : MonoBehaviour
{

    private int _maximalMagazinAmmo;
    private int _maximalReserveAmmo;

    private int _currentMagazinAmmo;
    private int _currentReserveAmmo;

    public int MaximalMagazinAmmo
    {
        get => _maximalMagazinAmmo;
        set => _maximalMagazinAmmo = value;
    }

    public int MaximalReserveAmmo
    {
        get => _maximalReserveAmmo;
        set => _maximalReserveAmmo = value;
    }

    public int CurrentMagazinAmmo
    {
        get => _currentMagazinAmmo;
        set => _currentMagazinAmmo = value;
    }

    public int CurrentReserveAmmo
    {
        get => _currentReserveAmmo;
        set => _currentReserveAmmo = value;
    }
}
