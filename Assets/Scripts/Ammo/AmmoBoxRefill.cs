using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBoxRefill : MonoBehaviour
{
    [SerializeField] List<AmmoBox> _ammoBoxes;
    // Start is called before the first frame update
    void Start()
    {
        _ammoBoxes = AmmoBox.AllAmmoBoxes;
        Debug.Log($"Ammolist: {_ammoBoxes.Count}");
    }
    
}
