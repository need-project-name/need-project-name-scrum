﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreboardMenu : MonoBehaviour
{

    public Button backButton;
    //Hier die Button Funktionen einbauen
    private void Start()
    {
        backButton.GetComponent<Button>().onClick.AddListener(BackEvent);
    }

    private void BackEvent()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
