using Assets.Scripts.Menu;
using ProjectZ.Scoreboards;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
namespace ProjectZ.Waves
{
    public class WaveController : MonoBehaviour
    {

        private int currentWave = 0;
        private bool nextIsFirstWave = true;
        private bool waveIsActice = false;
        private TextMeshProUGUI currentWaveObject;

        private int currentScore = 0;
        private TextMeshProUGUI currentScoreObject;

        private int enemiesAmount = 0;
        private int enemiesHealth = 0;
        private int enemiesKilled = 0;
        private WaveTimer waveTimer;
        private DeathScreenController deathScreenController;
        public static List<EnemySpawner> enemySpawner = new List<EnemySpawner>();

        // Start is called before the first frame update
        void Start()
        {

            waveTimer = GameObject.Find("WaveController").GetComponent<WaveTimer>();
            deathScreenController = GameObject.Find("DeathScreenController").GetComponent<DeathScreenController>();

            currentWaveObject = GameObject.Find("CurrentPlayerWave_Text").GetComponent<TextMeshProUGUI>();
            currentScoreObject = GameObject.Find("CurrentPlayerScore_Text").GetComponent<TextMeshProUGUI>();
            ResetGame();
        }

        public void ResetGame()
        {
            deathScreenController.HideDeathScreen();
            nextIsFirstWave = true;
            DisplayCurrentWave();
            DisplayCurrentScore();
            waveTimer.StartTimer(30);
        }

        void Update()
        {
            if( !waveTimer.timerIsRunning && !waveIsActice)
            {
                if (nextIsFirstWave)
                    StartWave();
                else
                    NextWave();
            }
        }

        private void StartSpawnEnemiesThread()
        {
            try
            {
                Thread thread = new Thread(new ThreadStart(SpawnEnemies));
                thread.Start();
            }
            catch(Exception e)
            {
                Debug.Log(e.ToString());
            }
            
        }

        private void SpawnEnemies()
        {
            for (int i = 0; i < enemiesAmount; i++)            {
                Thread.Sleep(new System.Random().Next(100, 1000));
                SpawnEnemy();
            }
        }
        private void SpawnEnemy()
        {
            EnemySpawner spawner = enemySpawner[new System.Random().Next(0, enemySpawner.Count - 1)];
            spawner.SpawnEnemy();
            Debug.Log("Enemie spawned!");
        }
        private void StartWave()
        {

            Debug.Log("Started!");
            waveIsActice = true;
            enemiesAmount = 5;
            enemiesKilled = 0;
            enemiesHealth = 100;
            EnemyHealth.baseHealth = enemiesHealth;
            currentWave = 1;
            DisplayCurrentWave();
            currentScore = 0;
            DisplayCurrentScore();
            StartSpawnEnemiesThread();
            //    PlayerDies();
            nextIsFirstWave = false;
        }
        
        private void NextWave()
        {
            Debug.Log("NextWave!");
            waveIsActice = true;
            currentWave++;
            enemiesAmount += 3;
            if (currentWave % 5 == 0)            
                enemiesHealth += 10;            
            enemiesKilled = 0;
            DisplayCurrentWave();
            StartSpawnEnemiesThread();
        }

        void DisplayCurrentWave()
        {
            currentWaveObject.text = currentWave.ToString();
        }
        void IncreaseScore(int socreToIncrease)
        {
            currentScore += socreToIncrease;
            DisplayCurrentScore();
        }
        void DisplayCurrentScore()
        {
            currentScoreObject.text = currentScore.ToString("D5");
        }
        public void EnemieDies(bool wasShot = true)
        {
            Debug.Log("EnemieDied!");
            enemiesKilled++;
            if (wasShot)
                IncreaseScore(1);
            else
                IncreaseScore(2);
            if ( enemiesAmount <= enemiesKilled )
            {
                IncreaseScore(10);
                waveTimer.StartTimer(10);
                waveIsActice = false;
            }

        }

        public void PlayerDies()
        {
            deathScreenController.ShowDeathScreen(currentScore);
        }

        public int getCurrentWave => currentWave;
    }
}
