﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveTimer : MonoBehaviour
{
    public float timeRemaining = 0;
    public bool timerIsRunning = false;
    public TextMeshProUGUI timeTextObject;

    private void Start()
    {

    }
    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime();
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
                timeTextObject.enabled = false;
            }
        }
    }
    public void StartTimer(float time)
    {
        timeTextObject.enabled = true;
        timeRemaining = time;
        timerIsRunning = true;
    }

    void DisplayTime() {
        timeTextObject.text = timeRemaining.ToString("N0");
    }
}