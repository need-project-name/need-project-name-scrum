using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Menu;
using ProjectZ.Waves;
using UnityEngine;

public class GunScript : MonoBehaviour {
    public AudioSource shootingSound;
    public ParticleSystem muzzleParticleSystem;

    public bool shooting;

    public int currentAmmo = 0;
    public int currentReserveAmmo = 0;

    public int maxAmmo = 30;
    public int maxReserve = 300;

    public bool isMelee = false;

    public double fireRate = 0.2;
    private double lastShot = 0.0;

    public int damage = 12;

    

    public void shoot() {
        if( currentAmmo != 0 ) {
            if( Time.time > fireRate + lastShot ) {
                Hit();
                muzzleFlashStart();
                playShootingSound();
                shooting = true;
                lastShot = Time.time;
                currentAmmo -= 1;
            }
        }
    }

    private void muzzleFlashStart() {
        muzzleParticleSystem.Play();
        ParticleSystem.EmissionModule em = muzzleParticleSystem.emission;
        em.enabled = true;
    }

    private void playShootingSound() {
        shootingSound.Play();
    }

    private void Hit() {
        RaycastHit hitInfo;
        Camera playerCam = GameObject.Find( "PlayerCamera" ).GetComponent<Camera>();
        Ray r = new Ray( playerCam.transform.position, playerCam.transform.forward );
        if( Physics.Raycast( r, out hitInfo ) ) {
            if( hitInfo.transform.CompareTag( "Enemy" ) ) {
                GameObject enemy = hitInfo.transform.gameObject;
                EnemyHealth enemyHealth = enemy.GetComponentInParent<EnemyHealth>();
                ParticleSystem enemyHitParticle = enemy.GetComponentInChildren<ParticleSystem>();

                enemyHitParticle.Play();
                ParticleSystem.EmissionModule emissionModule = enemyHitParticle.emission;
                emissionModule.enabled = true;

                enemyHealth.Health -= damage;
                if( enemyHealth.Health <= 0 ) {
                    Destroy( enemy.transform.parent.gameObject );

                    GameObject.Find( "WaveController" ).GetComponent<WaveController>().EnemieDies( !isMelee );

                }
            }
        }
    }

}
