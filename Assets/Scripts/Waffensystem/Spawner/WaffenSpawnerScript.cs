using System.Collections;
using System.Collections.Generic;
using ProjectZ.Waves;
using UnityEngine;

public class WaffenSpawnerScript : MonoBehaviour
{
    [SerializeField] private int spawnOnWave;
    [SerializeField] private GameObject WeaponToSpawn;
    
    private WaveController _WaveController;

    private bool isSpawned = false;
    
    // Start is called before the first frame update
    void Start()
    {
        _WaveController = GameObject.Find("WaveController").GetComponent<WaveController>(); 
        
    }

    // Update is called once per frame
    void Update()
    {
       spawnWeapon();
    }

    private void spawnWeapon()
    {
        if (spawnOnWave <= _WaveController.getCurrentWave && !isSpawned)
        {
            var transform1 = transform;
            Instantiate(WeaponToSpawn, transform1.position,transform1.rotation);
            isSpawned = true;
        }
    }
}
