using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using Random = UnityEngine.Random;

public class PickUpController : MonoBehaviour
{
    public GunScript GunScript;
    public Rigidbody rb;
    public Collider coll;
    private Transform player, gunContainer, cam, meleeContainer;

    private float pickUpRange = 10;
    private float dropForwardForce = 2, dropUpwardForce = 2;

    public bool equipped;
    public bool isMelee;
    
    public static bool slotFull;
    public static bool meleeSlotFull;

    private Vector3 distanceToPlayer;

    // Start is called before the first frame update
    void Start()
    {
        
        
        player = GameObject.FindGameObjectWithTag("Player").transform;
        cam = GameObject.Find("PlayerCamera").transform;
        gunContainer = GameObject.Find("GunContainer").transform;
        meleeContainer = GameObject.Find("MeleeContainer").transform;
        
        if (!equipped)
        {
            GunScript.enabled = false;
            rb.isKinematic = false;
            coll.isTrigger = false;
        }
        if (equipped)
        {
            GunScript.enabled = true;
            rb.isKinematic = true;
            coll.isTrigger = true;
            if (isMelee)
            {
                meleeSlotFull = true;
            }
            else
            {
                slotFull = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        distanceToPlayer = player.position - transform.position;
    }

    public void PickUp()
    {
        if (!equipped && distanceToPlayer.magnitude <= pickUpRange && !slotFull && !isMelee)
        {
            equipped = true;
            slotFull = true;
            
            transform.SetParent(gunContainer);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(Vector3.zero);
            transform.localScale = Vector3.one;

            rb.isKinematic = true;
            coll.isTrigger = true;

            GunScript.enabled = true;
        }
        
        if (!equipped && distanceToPlayer.magnitude <= pickUpRange && !meleeSlotFull && isMelee)
        {
            equipped = true;
            meleeSlotFull = true;
            
            transform.SetParent(meleeContainer);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(Vector3.zero);
            transform.localScale = Vector3.one;
            
            rb.isKinematic = true;
            coll.isTrigger = true;

            GunScript.enabled = true;
        }
    }

    public void Drop()
    {
        if (isMelee)
        {
            meleeSlotFull = false;
        }
        else
        {
          slotFull = false;  
        }
        equipped = false;
        
        
        transform.SetParent(null);

        rb.isKinematic = false;
        coll.isTrigger = false;

        rb.velocity = player.GetComponent<Rigidbody>().velocity + Vector3.one;
        
        rb.AddForce(cam.forward * dropForwardForce, ForceMode.Impulse);
        rb.AddForce(cam.up * dropUpwardForce, ForceMode.Impulse);

        float random = Random.Range(-1, 1);
        rb.AddTorque(new Vector3(random,random,random) * 10);

        GunScript.enabled = false;
    }
}