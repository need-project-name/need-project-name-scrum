using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor( typeof( TerrainGeneratorManager ) )]
public class TerrainGeneratorEditor : Editor {
    public override void OnInspectorGUI() {
        //base.OnInspectorGUI();

        DrawDefaultInspector();

        if( GUILayout.Button( "Generate Terrain" ) ) {
            TerrainGeneratorManager manager = serializedObject.targetObject as TerrainGeneratorManager;

            manager.GenerateWorld();
        }

        if( GUILayout.Button( "Generate Textures" ) ) {
            TerrainGeneratorManager manager = serializedObject.targetObject as TerrainGeneratorManager;

            manager.GenerateTextures();
        }
    }
}
