using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunAnimation : MonoBehaviour
{

    public Animator gunAnimatior;

    public PlayerMotor motor;
    // Start is called before the first frame update
    void Start()
    {
        gunAnimatior = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (motor.shooting)
        {
            gunAnimatior.SetTrigger("shoot");
        }
    }
}
