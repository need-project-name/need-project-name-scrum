using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class walkingAnimation : MonoBehaviour
{
    public Animator walkingAnim;

    public PlayerMotor motor;
    // Start is called before the first frame update
    void Start()
    {
        walkingAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (motor.running)
        {

            walkingAnim.SetTrigger("running");
        }
    }
}

