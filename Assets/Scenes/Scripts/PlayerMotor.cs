using Assets.Scripts.Menu;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using UnityEngine.UI;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerMotor : MonoBehaviour
{
    [SerializeField] private Camera playerCam;
    [SerializeField] private VoiceController voiceController;
    private PickUpController current_Gun, current_Melee;
    private GameObject current_GunObject, current_MeleeObject;
    private CharacterController charController;
    private InputManager inputManager;
    private AmmoDao _ammoDao;
    public StaminaBar stamina;
    public AudioSource reloadsource;

    public bool running;


    private Vector3 playerVelocity;

    private bool _currentUsedWeaponIsGun;

    private bool isGrounded;
    public bool sprinting;
    private bool lerpcrouch;

    private List<Collider> ammoBoxList = new List<Collider>();

    public bool crouching;

    public bool attack;

    public float speed = 5f;

    private float gravity = -9.8f;
    public float jumpHeight = 3f;

    public float crouchTimer = 3f;

    public bool shooting;
    

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        charController = GetComponent<CharacterController>();
        inputManager = GetComponent<InputManager>();
        _ammoDao = GetComponent<AmmoDao>();

        GameObject tempGunContainer = GameObject.FindWithTag("GunContainer");
        GameObject tempMeleeContainer = GameObject.FindWithTag("MeleeContainer");

        if (tempGunContainer.transform.childCount > 0)
        {
            current_GunObject = tempGunContainer.transform.GetChild(0).gameObject;
            current_Gun = current_GunObject.GetComponent<PickUpController>();
            _ammoDao.MaximalMagazinAmmo = current_Gun.GunScript.maxAmmo;
            _ammoDao.MaximalReserveAmmo = current_Gun.GunScript.maxReserve;
            _ammoDao.CurrentReserveAmmo = current_Gun.GunScript.currentReserveAmmo;
            _ammoDao.CurrentMagazinAmmo = current_Gun.GunScript.currentAmmo;
            current_GunObject.SetActive(true);
            _currentUsedWeaponIsGun = true;
        }

        if (tempMeleeContainer.transform.childCount > 0)
        {
            current_MeleeObject = tempMeleeContainer.transform.GetChild(0).gameObject;
            current_Melee = current_MeleeObject.GetComponent<PickUpController>();
            current_MeleeObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (DeathScreenController.IsPaused)
            inputManager.OnDisable();
        isGrounded = charController.isGrounded;

        if (lerpcrouch)
        {
            crouchTimer += Time.deltaTime;
            float p = crouchTimer / 1;
            p *= p;
            if (crouching)
            {
                charController.height = Mathf.Lerp(charController.height, 1, p);
                speed = 2f;
            }
            else
            {
                charController.height = Mathf.Lerp(charController.height, 2, p);
                if (sprinting)
                {
                    speed = 8f;
                }
                else
                {
                    speed = 5f;
                }
            }

            if (p > 1)
            {
                lerpcrouch = false;
                crouchTimer = 0f;
            }
        }

        if (!stamina.allowSprinting && !crouching && stamina.staminabar.value < 100)
        {
            stamina.refillStaminaBar();
            speed = 5f;
        }

    }

    private void FixedUpdate()
    {
        if (!DeathScreenController.IsPaused)
        {
            shooting = false;
            if (attack)
            {
                if (_ammoDao.CurrentMagazinAmmo > 0)
                {
                    current_Gun.GunScript.shoot();
                    shooting = true;
                    _ammoDao.CurrentMagazinAmmo = current_Gun.GunScript.currentAmmo;
                }
            }

        }
    }

    // gets inputs from InputManager.cs and applys them to charactercontroller
    public void ProcessMove(Vector2 input)
    {
        Vector3 moveDirection = Vector3.zero;
        moveDirection.x = input.x;
        moveDirection.z = input.y;
        charController.Move(transform.TransformDirection(moveDirection) * (speed * Time.deltaTime));
        playerVelocity.y += gravity * Time.deltaTime;

        if (isGrounded && playerVelocity.y < 0)
        {
            playerVelocity.y = -2f;
        }
        charController.Move(playerVelocity * Time.deltaTime);
    }


    public void Jump()
    {
        if (isGrounded)
        {
            playerVelocity.y = Mathf.Sqrt(jumpHeight * -3f * gravity);
            if (voiceController.IsFemale)
            {
                GameObject.Find("FemaleJumpAndLand").GetComponent<AudioSource>().Play();
            }
            else
            {
                GameObject.Find("MaleJumpAndLand").GetComponent<AudioSource>().Play();
            }
        }
    }

    public void Sprint()
    {
        sprinting = !sprinting;
        running = false;
        if (sprinting && !crouching && stamina.allowSprinting)
        {
            speed = 7f;
            running = true;

        }
        else if ((!sprinting && !crouching) || (!stamina.allowSprinting && !crouching))
        {
            speed = 5f;
            running = false;
        }
    }

    public void Crouch()
    {
        crouching = !crouching;
        crouchTimer = 0f;
        lerpcrouch = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);
        if (other.gameObject.name == "AmmoBoxRefillable(Clone)")
        {
            ammoBoxList.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "AmmoBoxRefillable(Clone)")
        {
            ammoBoxList.Remove(other);
        }
    }

    public void RefillAmmo()
    {
        if (ammoBoxList.Count > 0)
        {
            _ammoDao.CurrentReserveAmmo = _ammoDao.MaximalReserveAmmo;
            current_Gun.GunScript.currentReserveAmmo = current_Gun.GunScript.maxReserve;
        }
    }

    public void Reload()
    {
        if (_ammoDao.CurrentReserveAmmo > 0)
        {
            _ammoDao.CurrentReserveAmmo += _ammoDao.CurrentMagazinAmmo;
            _ammoDao.CurrentMagazinAmmo = 0;
            if (_ammoDao.CurrentReserveAmmo >= _ammoDao.MaximalMagazinAmmo)
            {
                _ammoDao.CurrentMagazinAmmo = _ammoDao.MaximalMagazinAmmo;
                _ammoDao.CurrentReserveAmmo -= _ammoDao.MaximalMagazinAmmo;

                current_Gun.GunScript.currentAmmo = _ammoDao.CurrentMagazinAmmo;
                current_Gun.GunScript.currentReserveAmmo = _ammoDao.CurrentReserveAmmo;
                reloadsource.Play();
            }
            else
            {
                _ammoDao.CurrentMagazinAmmo = _ammoDao.CurrentReserveAmmo;
                _ammoDao.CurrentReserveAmmo = 0;
                
                current_Gun.GunScript.currentAmmo = _ammoDao.CurrentMagazinAmmo;
                current_Gun.GunScript.currentReserveAmmo = _ammoDao.CurrentReserveAmmo;
            }
        }
    }

    public void changeWeapon(int input)
    {
        switch (input)
        {
            case 1:
                if (!_currentUsedWeaponIsGun)
                {
                    switchWeapon();
                }
                break;
            case 2:
                if (_currentUsedWeaponIsGun)
                {
                    switchWeapon();
                }
                break;
        }
    }

    public void switchWeapon()
    {
        if (current_GunObject != null && current_MeleeObject != null)
        {
            if (_currentUsedWeaponIsGun)
            {
                current_GunObject.SetActive(false);
                current_MeleeObject.SetActive(true);
                _currentUsedWeaponIsGun = false;
            }
            else
            {
                current_MeleeObject.SetActive(false);
                current_GunObject.SetActive(true);
                _currentUsedWeaponIsGun = true;
            }   
        }
    }

    public void Pickup()
    {
        RaycastHit hitInfo;
        Ray r = new Ray(playerCam.transform.position, playerCam.transform.forward);

        if (Physics.Raycast(r, out hitInfo))
        {
            //if it is tagged as a weapon
            if (hitInfo.transform.CompareTag("Weapon"))
            {
                PickUpController tempController = hitInfo.transform.gameObject.GetComponent<PickUpController>();
                if (tempController.isMelee)
                {
                    if (current_Melee)
                    {
                        current_Melee.Drop();
                    }

                    current_Melee = tempController;
                    current_MeleeObject = hitInfo.transform.gameObject;
                    current_Melee.PickUp();
                }
                else
                {
                    if (current_Gun)
                    {
                        current_Gun.Drop();
                    }
                    current_Gun = tempController;
                    current_GunObject = hitInfo.transform.gameObject;
                    current_Gun.PickUp();
                    _ammoDao.MaximalMagazinAmmo = current_Gun.GunScript.maxAmmo;
                    _ammoDao.MaximalReserveAmmo = current_Gun.GunScript.maxReserve;
                    _ammoDao.CurrentReserveAmmo = current_Gun.GunScript.currentReserveAmmo;
                    _ammoDao.CurrentMagazinAmmo = current_Gun.GunScript.currentAmmo;
                }
            }
        }
    }
}