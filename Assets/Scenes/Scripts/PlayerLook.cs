using Assets.Scripts.Menu;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{

    private float xrotation = 0f;
    public Camera cam;
    public float xSensitivty = 30f;
    public float ySensitivty = 30f;

    // Start is called before the first frame update
    public void processLook(Vector2 input)
    {

        if (!DeathScreenController.IsPaused)
        {
            float mouseX = input.x;
            float mouseY = input.y;

            xrotation -= (mouseY * Time.deltaTime) * ySensitivty;
            xrotation = Mathf.Clamp(xrotation, -80f, 80f);

            cam.transform.localRotation = Quaternion.Euler(xrotation, 0, 0);
            transform.Rotate(Vector3.up * (mouseX * Time.deltaTime) * xSensitivty);
        }
    }
}
