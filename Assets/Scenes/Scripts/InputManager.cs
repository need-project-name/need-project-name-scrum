using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{

    private PlayerInput playerInput;
    private PlayerInput.OnFootActions onFoot;
    private PlayerMotor motor;
    private PlayerLook look;


    // Start is called before the first frame update
    void Awake()
    {
        playerInput = new PlayerInput();

        motor = GetComponent<PlayerMotor>();
        look = GetComponent<PlayerLook>();

        onFoot = playerInput.OnFoot;
        onFoot.Jump.performed += ctx => motor.Jump();
        onFoot.Sprint.performed += ctx => motor.Sprint();
        onFoot.Crouch.performed += ctx => motor.Crouch();
        onFoot.RefillAmmo.performed += ctx => motor.RefillAmmo();
        onFoot.Reload.performed += ctx => motor.Reload();
        onFoot.Pickup.performed += ctx => motor.Pickup();
        onFoot.switchWeapon.performed += ctx => motor.switchWeapon();
        onFoot.switchWeaponScroll.performed += ctx => motor.switchWeapon();
        onFoot.switchToGun.performed += ctx => motor.changeWeapon(1);
        onFoot.switchToMelee.performed += ctx => motor.changeWeapon(2);
        onFoot.Attack.started += ctx => motor.attack = true;
        onFoot.Attack.canceled += ctx => motor.attack = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        motor.ProcessMove(onFoot.Movement.ReadValue<Vector2>());
    }

    private void Update()
    {
        look.processLook(onFoot.Look.ReadValue<Vector2>());
    }

    public void OnEnable()
    {
        onFoot.Enable();
    }

    public void OnDisable()
    {
        onFoot.Disable();
    }
}
